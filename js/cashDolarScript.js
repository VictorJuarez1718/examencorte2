// Evento change

function change(){
    const monedaOrigen = document.getElementById('monedaOrigen');
    const monedaDestino = document.getElementById('monedaDestino');

    let select1 = "";
    let select2 = "";

    // Deshabilitar la opción pesos mexicanos
    if(parseInt(monedaOrigen.value) == 1){
        select1 =   `<option value="6">Dólar estadounidense</option>
                    <option value="7">Dolar canadiense</option>
                    <option value="8">Euro</option>`;
        select2 =   `<option value="1" selected>Peso Mexicano</option>
                    <option value="2">Dolar estadounidense</option>
                    <option value="3">Dolar canadiense</option>
                    <option value="4">Euro</option>`;
    // Deshabilitar la opción dólares americanos
    }else if(parseInt(monedaOrigen.value) == 2){
        select1 =   `<option value="5">Peso mexicano</option>
                     <option value="7">Dolar canadiense</option>
                     <option value="8">Euro</option>`;
        select2 =   `<option value="1">Peso Mexicano</option>
                    <option value="2" selected>Dolar estadounidense</option>
                    <option value="3">Dolar canadiense</option>
                    <option value="4">Euro</option>`;
    }
    // Deshabilitar la opción dólares canadienses
    else if(parseInt(monedaOrigen.value) == 3){
        select1 =   `<option value="5">Peso mexicano</option>
                    <option value="6">Dolar estadounidense</option>
                    <option value="8">Euro</option>`;
        select2 =   `<option value="1">Peso Mexicano</option>
                    <option value="2">Dolar estadounidense</option>
                    <option value="3" selected>Dolar canadiense</option>
                    <option value="4">Euro</option>`;
    }
    // Deshabilitar la opción euros
    else if(parseInt(monedaOrigen.value) == 4){
        select1 =  `<option value="5">Peso mexicano</option>
                    <option value="6">Dolar estadounidense</option>
                    <option value="7">Dolar canadiense</option>`;
        select2 =   `<option value="1">Peso Mexicano</option>
                    <option value="2">Dolar estadounidense</option>
                    <option value="3">Dolar canadiense</option>
                    <option value="4" selected>Euro</option>`;
    }
    
    monedaDestino.innerHTML = select1;
    monedaOrigen.innerHTML = select2;


}

// Función para calcular
function calcular(){
    let cantidad = document.getElementById('cantidad').value;
    let monedaOrigen = document.getElementById('monedaOrigen');
    let monedaDestino = document.getElementById('monedaDestino');

    let subtotal = document.getElementById('subTotal');
    let comision = document.getElementById('comision');
    let total = document.getElementById('Totalpagar');

    // Pesos mexicanos a otras monedas
    if(parseInt(monedaOrigen.value) == 1 && parseInt(monedaDestino.value) == 6){
        subtotal.value = cantidad / 19.85;
    }else if(parseInt(monedaOrigen.value) == 1 && parseInt(monedaDestino.value) == 7){
        subtotal.value = (cantidad / 19.85) * 1.35;
    }else if(parseInt(monedaOrigen.value) == 1 && parseInt(monedaDestino.value) == 8){
        subtotal.value = (cantidad / 19.85) * 0.99;
    }

    //Dólares americanos a otras monedas
    else if(parseInt(monedaOrigen.value) == 2 && parseInt(monedaDestino.value) == 5){
            subtotal.value = cantidad * 19.85;
    }else if(parseInt(monedaOrigen.value) == 2 && parseInt(monedaDestino.value) == 7){
        subtotal.value = cantidad * 1.35;
    }else if(parseInt(monedaOrigen.value) == 2 && parseInt(monedaDestino.value) == 8){
        subtotal.value = cantidad * 0.99;
    }

    // Dólares canadiences a otras monedas
    else if(parseInt(monedaOrigen.value) == 3 && parseInt(monedaDestino.value) == 5){
        subtotal.value = (cantidad/1.35) *19.85;
    }else if(parseInt(monedaOrigen.value) == 3 && parseInt(monedaDestino.value) == 6){
        subtotal.value = cantidad / 1.35;
    }else if(parseInt(monedaOrigen.value) == 3 && parseInt(monedaDestino.value) == 8){
        subtotal.value = (cantidad/1.35) * 0.99;
    }

    // Euros a otras monedas
    else if(parseInt(monedaOrigen.value) == 4 && parseInt(monedaDestino.value) == 5){
        subtotal.value = (cantidad / 0.99) * 19.85;
    }else if(parseInt(monedaOrigen.value) == 4 && parseInt(monedaDestino.value) == 6){
        subtotal.value = cantidad * 0.99;
    }else if(parseInt(monedaOrigen.value) == 4 && parseInt(monedaDestino.value) == 7){
        subtotal.value = (cantidad / 0.99) * 1.35;
    }

    comision.value = (subtotal.value * 0.03).toFixed(2);
    total.value = (parseFloat(subtotal.value) + parseFloat(comision.value)).toFixed(2);
    


}

let subtotalF = 0
let comisionF = 0
let pagarF = 0

function registrar(){
    let cantidad = document.getElementById('cantidad').value;
    let monedaOrigen = document.getElementById('monedaOrigen');
    let monedaDestino = document.getElementById('monedaDestino');
    let subtotal = document.getElementById('subTotal');
    let comision = document.getElementById('comision');
    let total = document.getElementById('Totalpagar');

    let registro = document.getElementById('registros');
    let totales = document.getElementById('Totales');

    if(parseInt(monedaOrigen.value) == 1){
        monedaOrigen = "Peso(s) Mexicanos";
    }else if(parseInt(monedaOrigen.value) == 2){
        monedaOrigen = "Dólar(es) estadounidense";
    }else if(parseInt(monedaOrigen.value) == 3){
        monedaOrigen = "Dólar(es) canadiense";
    }else if(parseInt(monedaOrigen.value) == 4){
        monedaOrigen = "Euro(s)";
    }

    if(parseInt(monedaDestino.value) == 5){
        monedaDestino = "Peso(s) Mexicanos"
    }else if(parseInt(monedaDestino.value) == 6){
        monedaDestino = "Dólar(es) estadounidense"
    }else if(parseInt(monedaDestino.value) == 7){
        monedaDestino = "Dólar(es) canadiense"
    }else if(parseInt(monedaDestino.value) == 8){
        monedaDestino = "Euro"
    }
    

    registro.innerHTML += "Se cambiaron " + cantidad  + " " + monedaOrigen + " a" + " " + monedaDestino; 
    
    subtotalF += parseFloat(subtotal.value);
    comisionF += parseFloat(comision.value);
    pagarF += parseFloat(total.value);
    
    totales.innerHTML = "subtotal: " + subtotalF + " comisión: " + comisionF + "Total: " + pagarF ; 


}

function borrar(){
    
    let registro = document.getElementById('registros');
    let subtotal = document.getElementById('subTotal');
    let comision = document.getElementById('comision');
    let total = document.getElementById('Totalpagar');
    let totales = document.getElementById('Totales');
    let cantidad = document.getElementById('cantidad').value;

    cantidad.innerHTML = "";
    registro.innerHTML = "";
    subtotal.value = "";
    comision.value = "";
    total.value = "";
    totales.innerHTML = "";

}